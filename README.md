# Small Table Loader Sif
Simple indent format. Tabs (`\t`) and newlines (`\n`) encode tables

# API

* `table sif.parse(str)` - Parse a string
* `table sif.read(path)` - Parse a file

# Examples

## List

```
moods
	good
	great
	fantastic
```
```lua
{
	moods = {
		"good", "great", "fantastic"
	}
}
```

## Newline separated list

```
gott
flott
frábært
```
```lua
{ "gott", "flott", "frábært" }
```

## Key/Value pairs

```
font
	name
		Source Code Pro
	size
		32
```
```lua
{
	font = {
		name = "Source Code Pro",
		size = "32"
	}
}
```

