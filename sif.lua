-- This Source Code Form is subject to https://mozilla.org/MPL/2.0/
local sif = {}

local function escapeContent(content)
	return content:gsub("\\\t", "\t")
end

function sif.parse(str)
	local root = {}
	local currentIndent = root
	local indentStack = { root }
	local lastItem = nil
	local lastLastItem = nil
	local linen = 0
	local function popStack()
		local oldStack = table.remove(indentStack)
		local alreadySeen1 = false
		for i in pairs(oldStack) do
			if alreadySeen1 then
				goto isNotReducable
			end
			if i == 1 then
				alreadySeen1 = true
			else
				goto isNotReducable
			end
		end
		if not indentStack[#indentStack] then
			return
		end
		indentStack[#indentStack][lastLastItem] = oldStack[1]
		::isNotReducable::
		currentIndent = indentStack[#indentStack]
	end
	for line in str:gmatch("[^\n]+") do
		linen = linen + 1
		local firstChr = line:sub(1, 1)
		if firstChr ~= "" then
			local indentLevel    = #line:match("^\t*")
			local content        = escapeContent(line:sub(indentLevel + 1))
			local stackLevelDiff = (indentLevel+1) - #indentStack
			if stackLevelDiff == 0 then
				table.insert(currentIndent, content)
			elseif stackLevelDiff == 1 then
				local nextIndent = { content }
				currentIndent[#currentIndent] = nil
				currentIndent[lastItem] = nextIndent
				table.insert(indentStack, nextIndent)
				currentIndent = nextIndent
			elseif stackLevelDiff > 1 then
				return nil, "Invalid indent"
			else
				for _=1,-stackLevelDiff do
					popStack()
				end
				table.insert(currentIndent, content)
			end
			lastLastItem = lastItem
			lastItem = content
		end
	end
	if next(indentStack[1]) then
		for _=1,#indentStack do
			popStack()
		end
	end

	return root
end
function sif.read(path)
	return sif.parse(assert(io.open(path, "r")):read("*a"))
end

return sif
