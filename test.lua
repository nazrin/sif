-- This Source Code Form is subject to https://mozilla.org/MPL/2.0/
local sif = require("sif")

local function test(str, correct)
	local tbl = sif.parse(str)
	local function check(tbl, correct)
		for k,v in pairs(tbl) do
			local typ = type(v)
			if typ == "table" then
				check(v, correct[k])
			else
				local cv = correct[k]
				assert(v == correct[k], ("%q ≠ %q"):format(v, cv))
			end
		end
	end
	check(tbl, correct)
end
local function testf(path, correct)
	local str = io.open(path):read("*a")
	test(str, correct)
end

test("",       {})
test("s",      { "s" })
test("s\t",    { "s\t" })
test("s\tx",   { "s\tx" })
test("s\n\tx", { s = "x" })
test("\ns\n",  { "s" })
test("s\n",    { "s" })
test("s\na",   { "s", "a" })

testf("test/1", { "1", "2", "3" })
testf("test/2", { moods = { "good", "great", "fantastic" } })
testf("test/3", { A = { B = "C" } })
testf("test/4", { A = "1", B = "2", C = { C1 = {"3", "4", "5"} } })


